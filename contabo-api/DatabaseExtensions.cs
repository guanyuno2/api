using MongoDB.Driver;

namespace contabo_api.Database;

public static class MongoProfilerExtensions
{
    public static IMongoDatabase GetAndProfileMongoDb(this string config)
    {
        MongoUrl mongoUrl = new MongoUrl(config);
        MongoClientSettings mongoClientSettings = MongoClientSettings.FromUrl(mongoUrl);
        var mongoDb = (new MongoClient(mongoUrl)).GetDatabase(mongoUrl.DatabaseName);
        return mongoDb;
    }
}