namespace contabo_api.Repositories;

public interface IFileRepository : IRepositoryDefault<MGFileModel>
{
    Task<List<MGFileModel>> GetMyFile(string id);
    Task<List<MGFileModel>> SearchFile(string query);
}