namespace contabo_api.Repositories;

public interface IFolderRepository : IRepositoryDefault<MGFolderModel>
{
    Task<List<MGFolderModel>> GetMyFolder(string id);
    Task<List<MGFolderModel>> GetAllMyFolder();
    Task<List<MGFolderModel>> SearchFolder(string query);
}