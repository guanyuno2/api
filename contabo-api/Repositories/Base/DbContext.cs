using MongoDB.Driver;

namespace contabo_api.Repositories;
public class DbContext
{
    private readonly IMongoDatabase _db;

    public DbContext(IMongoDatabase db)
    {
        _db = db;
    }
}

public interface IMongoDb
{
    IMongoDatabase Db { get; }
}
// public class MongoDb : IMongoDb
// {
//     public MongoDb(IMongoDatabase db)
//     {
//         Db = db;
//     }

//     public IMongoDatabase Db { get; private set; }
// }