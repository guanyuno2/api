using System.Linq.Expressions;
using System.Reflection;
using contabo_api.Infrastructure;
using MongoDB.Bson;
using MongoDB.Driver;

namespace contabo_api.Repositories;

public class RepositoryDefault<T> : IRepositoryDefault<T> where T : class, ITrackableEntity
{
    private IRequestContext rqContext;
    private readonly IMongoCollection<T> _collection;
    private readonly IMongoCollection<T> _collectionSecondary;
    public new IMongoCollection<T> collection
    {
        get { return _collection; }
    }
    public IMongoCollection<T> collectionWithSecondaryPreferred
    {
        get { return _collectionSecondary; }
    }
    public bool isContext
    {
        get; set;
    }
    public string _userId;

    public string userId
    {
        get { return _userId; }
    }


    public RepositoryDefault(IRequestContext context, IMongoDatabase db)
    {
        rqContext = context;
        _collection = db.GetCollection<T>(typeof(T).Name);
        _userId = context?.UserId ?? "";
        _collectionSecondary = _collection;
        /*db.GetCollection<T>(typeof(T).Name)
            .WithWriteConcern(new WriteConcern("majority", TimeSpan.FromSeconds(5)))
            .WithReadPreference(new ReadPreference(
                    ReadPreferenceMode.SecondaryPreferred,
                    new TagSet[] {
                        new TagSet(new Tag[] {
                            new Tag("disk", "ssd")
                        })
                    }
                    ));*/
    }


    public async Task<T> Clone(ObjectId id, UpdateDefinition<T> update)
    {
        if (id == null)
            throw new ArgumentException(nameof(id));

        var filter = Builders<T>.Filter.Where(m => m.Id == id);

        var data = await collection.Find(filter).FirstOrDefaultAsync();
        data.Id = ObjectId.GenerateNewId();

        return data;
    }

    public Task<T> GetById(ObjectId id)
    {
        if (id == null)
            throw new ArgumentException(nameof(id));

        var filter = Builders<T>.Filter.Where(m => m.Id == id);

        var data = collection.Find(filter).FirstOrDefaultAsync();
        return data;
    }
    public Task<T> GetById(string id)
    {
        if (id == null)
            throw new ArgumentException(nameof(id));
        var oid = ObjectId.Parse(id);
        var filter = Builders<T>.Filter.Where(m => m.Id == oid);

        var data = collection.Find(filter).FirstOrDefaultAsync();
        return data;
    }

    public Task<List<T>> GetById(IEnumerable<ObjectId> ids)
    {
        if (ids == null)
            throw new ArgumentException(nameof(ids));
        if (ids.Count<ObjectId>() == 0)
            return null;

        var filter = Builders<T>.Filter.Where(m => ids.Contains(m.Id));

        var data = collection.Find(filter).ToListAsync();
        return data;
    }

    public Task<List<T>> GetById(List<string> ids)
    {
        if (ids == null)
            throw new ArgumentException(nameof(ids));
        if (ids.Count<string>() == 0)
            return null;
        var listIds = new List<ObjectId>();

        ids.ForEach(m =>
        {
            if (!string.IsNullOrEmpty(m))
            {
                listIds.Add(ObjectId.Parse(m));
            }
        });

        listIds = listIds.Distinct().ToList();

        var filter = Builders<T>.Filter.Where(m => listIds.Contains(m.Id));

        var data = collection.Find(filter).ToListAsync();
        return data;
    }

    public Task Add(T entity, string createdBy)
    {
        if (entity.Id == null)
            entity.Id = ObjectId.GenerateNewId();
        entity.CreatedAt = DateTime.UtcNow;
        entity.UpdatedAt = DateTime.UtcNow;
        entity.CreatedBy = createdBy;
        entity.UpdatedBy = createdBy;

        return collection.InsertOneAsync(entity);
    }

    public Task<List<T>> GetManyByCondition(FilterDefinition<T> filter)
    {

        var data = collection.Find(filter).ToListAsync();
        return data;
    }
    public Task<long> Count(FilterDefinition<T> filter)
    {
        return collection.Find(filter).CountDocumentsAsync();
    }

    public Task Delete(ObjectId id)
    {
        var filter = Builders<T>.Filter.Where(m => m.Id == id);

        return this.collection.DeleteOneAsync(filter);
    }

    public Task Delete(List<ObjectId> ids)
    {
        var filter = Builders<T>.Filter.Where(m => ids.Contains(m.Id));

        return this.collection.DeleteOneAsync(filter);
    }

    public Task<T> GetOneByCondition(FilterDefinition<T> filter)
    {
        return collection.Find(filter).FirstOrDefaultAsync();
    }

    public Task UpdateByCondition(FilterDefinition<T> filter, UpdateDefinition<T> update, bool updateMany = false, bool appendTrackingInfo = true)
    {
        var updList = new List<UpdateDefinition<T>> { update };

        if (appendTrackingInfo)
            updList.Add(Builders<T>.Update.Set(x => x.UpdatedBy, userId)
                                            .Set(x => x.UpdatedAt, DateTime.UtcNow));

        if (updateMany)
            return collection.UpdateManyAsync(filter, Builders<T>.Update.Combine(updList));

        return collection.UpdateOneAsync(filter, Builders<T>.Update.Combine(updList));
    }

    public Task UpdateByCondition_Tracking(FilterDefinition<T> filter, UpdateDefinition<T> update, bool updateMany = false, bool appendTrackingInfo = true, string updatedBy = "", DateTime? updatedAt = null)
    {
        updatedBy = updatedBy ?? "";
        updatedAt = updatedAt ?? DateTime.UtcNow;

        var updList = new List<UpdateDefinition<T>> { update };
        if (appendTrackingInfo)
            updList.Add(Builders<T>.Update.Set(x => x.UpdatedBy, updatedBy)
                                            .Set(x => x.UpdatedAt, updatedAt));

        if (updateMany)
            return collection.UpdateManyAsync(filter, Builders<T>.Update.Combine(updList));

        return collection.UpdateOneAsync(filter, Builders<T>.Update.Combine(updList));
    }

    public Task DeleteByCondition(FilterDefinition<T> filter, bool deleteMany = false)
    {

        if (deleteMany)
            return collection.DeleteManyAsync(filter);

        return collection.DeleteOneAsync(filter);
    }

    public Task Add(T entity)
    {
        if (entity.Id == null || entity.Id == default)
            entity.Id = ObjectId.GenerateNewId();

        entity.CreatedAt = DateTime.UtcNow;
        entity.UpdatedAt = DateTime.UtcNow;
        entity.CreatedBy = this.userId;
        entity.UpdatedBy = this.userId;

        return collection.InsertOneAsync(entity);
    }

    public Task Delete(T entity)
    {
        if ((object)entity == null)
            throw new KeyNotFoundException();
        var filter = Builders<T>.Filter.Where(m => m.Id == entity.Id);

        return this.collection.DeleteOneAsync(filter);
    }

    public Task<List<T>> GetById(IEnumerable<string> ids)
    {
        return this.GetById(ids.ToList());
    }

    public Task Update(T entity, params Expression<Func<T, object>>[] properties)
    {
        ObjectId id = entity.Id;
        var filter = Builders<T>.Filter.Where(m => m.Id == entity.Id);

        entity.UpdatedAt = DateTime.UtcNow;

        if (properties == null || properties.Length == 0)
            return collection.ReplaceOneAsync(filter, entity);

        UpdateDefinition<T> update = (UpdateDefinition<T>)null;
        Type type = typeof(T);
        foreach (Expression<Func<T, object>> property in properties)
        {
            string propertyName = property.GetPropertyName<T, object>();
            if (propertyName == "UpdatedAt")
                update = __invoke(propertyName, DateTime.UtcNow, update);
            else if (propertyName == "UpdatedBy")
                update = __invoke(propertyName, _userId, update);
            else
                update = __invoke(propertyName, type.GetProperty(propertyName).GetValue((object)entity), update);
        }

        return this.collection.UpdateOneAsync(filter, update);
    }
    private UpdateDefinition<T> __invoke(string propertyName, object value, UpdateDefinition<T> update)
    {
        Type type = typeof(object);
        if (value != null)
            type = value.GetType();
        return (UpdateDefinition<T>)this.GetType().GetMethod("__set", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic).MakeGenericMethod(type).Invoke((object)this, new object[3]
        {
                 (object) propertyName,
                value,
                (object) update
        });
    }

}

public static class RepositoryExtension
{
    public static string GetPropertyName<T, TProperty>(this Expression<Func<T, TProperty>> expression)
    {
        MemberExpression memberExpression = null;
        if (expression.Body is UnaryExpression)
            memberExpression = (MemberExpression)((UnaryExpression)expression.Body).Operand;
        if (expression.Body is MemberExpression)
            memberExpression = expression.Body as MemberExpression;
        if (memberExpression == null)
            throw new InvalidOperationException("Expression must be a member expression");
        return memberExpression.Member.Name;
    }
}