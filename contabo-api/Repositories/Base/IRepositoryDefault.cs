using System.Linq.Expressions;
using contabo_api.Infrastructure;
using MongoDB.Bson;
using MongoDB.Driver;

namespace contabo_api.Repositories;

public interface IRepositoryDefault<T>
{
    string userId { get; }
    bool isContext { get; }
    IMongoCollection<T> collection { get; }
    IMongoCollection<T> collectionWithSecondaryPreferred { get; }
    Task<T> GetById(ObjectId id);
    Task<T> GetById(string id);
    Task<List<T>> GetById(IEnumerable<ObjectId> ids);
    Task<List<T>> GetById(List<string> ids);
    Task<T> GetOneByCondition(FilterDefinition<T> filter);
    Task<List<T>> GetManyByCondition(MongoDB.Driver.FilterDefinition<T> filter);
    Task<long> Count(FilterDefinition<T> filter);
    Task Add(T entity);
    Task Add(T entity, string createdBy);
    Task Update(T entity, params Expression<Func<T, object>>[] properties);
    Task Delete(T entity);
    Task Delete(ObjectId id);
    Task Delete(List<ObjectId> ids);
    Task UpdateByCondition(FilterDefinition<T> filter, UpdateDefinition<T> update, bool updateMany = false, bool appendTrackingInfo = true);
    Task UpdateByCondition_Tracking(FilterDefinition<T> filter, UpdateDefinition<T> update, bool updateMany = false, bool appendTrackingInfo = true, string updatedBy = "", DateTime? updatedAt = null);
    Task DeleteByCondition(FilterDefinition<T> filter, bool deleteMany = false);
}