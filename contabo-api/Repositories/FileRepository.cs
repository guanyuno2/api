using MongoDB.Bson;
using MongoDB.Driver;

namespace contabo_api.Repositories;

public class FileRepository : RepositoryDefaultAbstract<MGFileModel>, IFileRepository
{
    private readonly FilterDefinitionBuilder<MGFileModel> FILTER = Builders<MGFileModel>.Filter;
    public FileRepository(IRepositoryDefault<MGFileModel> repo) : base(repo)
    {

    }

    public Task<List<MGFileModel>> GetMyFile(string id)
    {
        var pid = ObjectId.TryParse(id, out ObjectId parentid) ? parentid : (ObjectId?)null;
        var filter = FILTER.And(
            FILTER.Eq(a => a.CreatedBy, userId),
            FILTER.Eq(a => a.ParentId, pid)
        );

        return collection.Find(filter).ToListAsync();
    }

    public Task<List<MGFileModel>> SearchFile(string query)
    {
        var filter = FILTER.And(
            FILTER.Eq(a => a.CreatedBy, userId)
        );
        if (!string.IsNullOrEmpty(query))
            filter &= FILTER.Text($"\"{query}\"");
        return collection.Find(filter).ToListAsync();
    }
}