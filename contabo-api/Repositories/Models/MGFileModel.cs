using contabo_api.Infrastructure;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace contabo_api.Repositories;

[BsonIgnoreExtraElements]
public class MGFileModel : TrackableEntity
{
    public string Url { get; set; } = "";
    public string FileName { get; set; } = "";
    public ObjectId? ParentId { get; set; }
}