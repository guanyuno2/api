using contabo_api.Infrastructure;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace contabo_api.Repositories;

[BsonIgnoreExtraElements]
public class MGFolderModel : TrackableEntity
{
    public string Name { get; set; } = "";
    public ObjectId? ParentId { get; set; }
}