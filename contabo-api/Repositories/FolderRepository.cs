using MongoDB.Bson;
using MongoDB.Driver;

namespace contabo_api.Repositories;

public class FolderRepository : RepositoryDefaultAbstract<MGFolderModel>, IFolderRepository
{
    private readonly FilterDefinitionBuilder<MGFolderModel> FILTER = Builders<MGFolderModel>.Filter;
    public FolderRepository(IRepositoryDefault<MGFolderModel> repo) : base(repo)
    {

    }

    public Task<List<MGFolderModel>> GetAllMyFolder()
    {
        var filter = FILTER.And(
            FILTER.Eq(a => a.CreatedBy, userId)
        );
        return collection.Find(filter).ToListAsync();
    }

    public Task<List<MGFolderModel>> GetMyFolder(string id)
    {
        var pid = ObjectId.TryParse(id, out ObjectId parentid) ? parentid : (ObjectId?)null;
        var filter = FILTER.And(
            FILTER.Eq(a => a.CreatedBy, userId),
            FILTER.Eq(a => a.ParentId, pid)
        );

        return collection.Find(filter).ToListAsync();
    }

    public Task<List<MGFolderModel>> SearchFolder(string query)
    {
        var filter = FILTER.And(
            FILTER.Eq(a => a.CreatedBy, userId)
        );
        if (!string.IsNullOrEmpty(query))
            filter &= FILTER.Text($"\"{query}\"");
        return collection.Find(filter).ToListAsync();
    }
}