using contabo_api.Business;
using contabo_api.Controllers.Extensions;
using contabo_api.Infrastructure;
using contabo_api.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace contabo_api.Controllers;

[ApiController]
public class FolderController : Controller
{
    private readonly IFolderBusiness _biz;
    public FolderController(IFolderBusiness biz)
    {
        _biz = biz;
    }

    [HttpGet]
    [Route("path/{id}")]
    [Authorize]
    public async Task<IActionResult> GetPath([FromRoute] string id)
    {
        var data = await _biz.GetPath(id);
        return this.ToResult(data);
    }

    [HttpGet]
    [Route("folder/search")]
    [Authorize]
    public async Task<IActionResult> SearchFolder([FromQuery] string query)
    {
        var data = await _biz.SearchFolder(query);
        return this.ToResult(data);
    }

    [HttpPost]
    [Route("folder")]
    [Authorize]
    public async Task<IActionResult> CreateFolder([FromBody] FolderModel body)
    {
        var data = await _biz.CreateFolder(body);
        return this.ToResult(data);
    }

    [HttpGet]
    [Route("myfolder")]
    [Authorize]
    public async Task<IActionResult> GetMyFolder([FromQuery] string? id = "")
    {
        var data = await _biz.GetMyFolder(id);
        return this.ToResult(data);
    }

    [HttpGet]
    [Route("folder/{id}")]
    [Authorize]
    public async Task<IActionResult> GetFolder([FromRoute] string? id = "")
    {
        var data = await _biz.GetFolder(id);
        return this.ToResult(data);
    }
}

