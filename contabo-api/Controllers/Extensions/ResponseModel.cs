using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace contabo_api.Controllers.Extensions;

public class ResponseModel
{
    [JsonProperty("error")]
    [JsonPropertyName("error")]
    public bool Error { get; set; }

    [JsonProperty("message", NullValueHandling = NullValueHandling.Ignore)]
    [JsonPropertyName("message")]
    public string Message { get; set; }

    [JsonProperty("error_code", NullValueHandling = NullValueHandling.Ignore)]
    [JsonPropertyName("error_code")]
    public string Error_Code { get; set; }

    public static ResponseModel<T> Create<T>(
      T data,
      bool error = false,
      string message = null,
      string error_code = null)
    {
      var responseModel = new ResponseModel<T>();
      responseModel.Error = error;
      responseModel.Message = message;
      responseModel.Data = data;
      responseModel.Error_Code = error_code;
      return responseModel;
    }

    public static ResponseModel Create(bool error = false, string message = null, string error_code = null) => new ResponseModel()
    {
        Error = error,
        Message = message,
        Error_Code = error_code
    };
}

public class ResponseModel<T> : ResponseModel
  {
    [JsonProperty("data")]
    [JsonPropertyName("data")]
    public T Data { get; set; }
  }