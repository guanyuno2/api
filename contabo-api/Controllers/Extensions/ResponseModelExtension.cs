using contabo_api.Infrastructure;
using Microsoft.AspNetCore.Mvc;

namespace contabo_api.Controllers.Extensions;

public static class ResponseModelExtensions
{
    public static IActionResult ToResult<T>(this Controller controller, T data = null) where T : class
    {
        IRequestContext service = controller.HttpContext.RequestServices.GetService<IRequestContext>();
        string message1 = string.Empty;
        if (service.IsError)
        {
            string message2 = service.Errors.Select<KeyValuePair<string, string>, string>((Func<KeyValuePair<string, string>, string>)(m => m.Value)).Aggregate<string>((Func<string, string, string>)((a, b) => a + ", " + b));
            string error_code = service.Errors.Select<KeyValuePair<string, string>, string>((Func<KeyValuePair<string, string>, string>)(m => m.Key)).Aggregate<string>((Func<string, string, string>)((a, b) => a + ", " + b));
            return (IActionResult)controller.Json((object)ResponseModel.Create(true, message2, error_code));
        }
        // if (service.Success.Any<KeyValuePair<string, string>>())
        //     message1 = service.Success.Select<KeyValuePair<string, string>, string>((Func<KeyValuePair<string, string>, string>)(m => m.Value)).Aggregate<string>((Func<string, string, string>)((a, b) => a + ", " + b));
        return (IActionResult)controller.Json((object)ResponseModel.Create<T>(data, message: message1));
    }
}