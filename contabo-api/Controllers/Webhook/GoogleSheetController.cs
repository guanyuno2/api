﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace contabo_api.Controllers.Webhook;

public class GoogleSheetController : Controller
{
    public GoogleSheetController()
    {
    }

    [AllowAnonymous]
    [HttpPost]
    [Route("google-sheet/webhook")]
    [Authorize]
    public async Task<IActionResult> PostPayload([FromBody] GoogleSheetPayload payload)
    {
        // Handle the payload here
        // For example, log the payload or process the data
        // write log here // writing payload to string
        var payloadString = JsonConvert.SerializeObject(payload);
        Console.WriteLine("payload" + payloadString);
        

        return Ok(new { message = "Payload received successfully" });
    }
}

public class GoogleSheetPayload
{
    public string EventType { get; set; }
    public string SheetName { get; set; }
    public List<Dictionary<string, string>> Data { get; set; }
}