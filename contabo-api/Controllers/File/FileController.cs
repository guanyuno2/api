using contabo_api.Business;
using contabo_api.Controllers.Extensions;
using contabo_api.Infrastructure;
using contabo_api.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace contabo_api.Controllers;

[ApiController]
public class FileController : Controller
{
    private readonly IFileBusiness _biz;
    public FileController(IFileBusiness biz)
    {
        _biz = biz;
    }

    [HttpPost]
    [Route("file")]
    [Authorize]
    public async Task<IActionResult> CreateFile([FromBody] FileModel body)
    {
        var data = await _biz.UploadFile(body);
        return this.ToResult(data);
    }

    [HttpGet]
    [Route("myfile")]
    [Authorize]
    public async Task<IActionResult> GetMyFile([FromQuery] string? id = "")
    {
        var data = await _biz.GetMyFile(id);
        return this.ToResult(data);
    }

    [HttpGet]
    [Route("file/search")]
    [Authorize]
    public async Task<IActionResult> SearchFile([FromQuery] string query)
    {
        var data = await _biz.SearchFile(query);
        return this.ToResult(data);
    }

}

