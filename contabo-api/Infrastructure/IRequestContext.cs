namespace contabo_api.Infrastructure;

public interface IRequestContext
{

    public string UserId { get; }

    public string UserName { get; }

    public string UserEmail { get; }

    public string AccessToken { get; }
    public bool IsError { get; }
    public void AddError(string msg);
    Dictionary<string, string> Errors { get; }
    // IReadOnlyDictionary<string, string> Success { get; }
}