using System;
using MongoDB.Bson;

namespace contabo_api.Infrastructure;
public class TrackableEntity : ITrackableEntity
{
    public DateTime? CreatedAt { get; set; }
    public DateTime? UpdatedAt { get; set; }
    public string CreatedBy { get; set; }
    public string UpdatedBy { get; set; }
    public ObjectId Id { get; set; }
}