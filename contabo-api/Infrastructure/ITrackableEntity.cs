
using MongoDB.Bson;

namespace contabo_api.Infrastructure;

public interface ITrackableEntity
{
    ObjectId Id { get; set; }
    DateTime? CreatedAt { get; set; }

    DateTime? UpdatedAt { get; set; }

    string CreatedBy { get; set; }

    string UpdatedBy { get; set; }
}