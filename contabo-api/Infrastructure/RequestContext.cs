using System.Security.Claims;
using Microsoft.Extensions.Primitives;

namespace contabo_api.Infrastructure;

public class RequestContext : IRequestContext
{
    public HttpContext Context { get; set; }
    public Dictionary<string, string> Errors { get; }
    public bool IsError => this.Errors.Any<KeyValuePair<string, string>>();

    public RequestContext()
    {
        Errors = new Dictionary<string, string>();
    }

    public string UserId
    {
        get
        {
            Claim claim = this.Context?.User?.FindFirst("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier") ?? this.Context?.User?.FindFirst("sub");
            return claim?.Value + "" ?? "";
        }
    }

    public string UserName => this.Context?.User?.FindFirst("name")?.Value;

    public string UserEmail => (this.Context?.User?.FindFirst("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress") ?? this.Context?.User?.FindFirst("email"))?.Value;

    public string AccessToken
    {
        get
        {
            StringValues? header = this.Context?.Request?.Headers["Authorization"];
            return (header.HasValue ? (string)header.GetValueOrDefault() : (string)null)?.Substring("Bearer ".Length);
        }
    }


    public void AddError(string msg) => Errors.Add(Guid.NewGuid().ToString("n"), msg);
}