using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerUI;

namespace contabo_api;

internal static class HostingExtensions
{
    public static void AddIoftyAuthenticate(this IServiceCollection services, IConfiguration Configuration)
    {
        services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                // .AddJwtBearer(options =>
                //     {
                //         // base-address of your identityserver
                //         options.Authority = Configuration["App:Authority"];

                //         // audience is optional, make sure you read the following paragraphs
                //         // to understand your options
                //         options.Audience = "api1";


                //         // it's recommended to check the type header to avoid "JWT confusion" attacks
                //         options.TokenValidationParameters.ValidTypes = new[] { "at+jwt" };
                //     });
                // JWT tokens
                .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, options =>
                {
                    options.Authority = Configuration["App:Authority"];
                    options.TokenValidationParameters.ValidTypes = new[] { "at+jwt" };
                    options.TokenValidationParameters.ValidateAudience = false;
                    // if token does not contain a dot, it is a reference token
                    // options.ForwardDefaultSelector = Selector.ForwardReferenceToken("introspection");
                })
                ;
    }
    public static void AddSwaggerDoc(
            this IServiceCollection services, IConfiguration Configuration)
    {
        services.AddSwaggerGen(c =>
        {
            c.SwaggerDoc("v1", new OpenApiInfo { Title = "Contabo api", Version = "v1" });
            c.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme()
            {
                Type = SecuritySchemeType.OAuth2,
                Scheme = JwtBearerDefaults.AuthenticationScheme,
                Flows = new OpenApiOAuthFlows()
                {
                    Implicit = new OpenApiOAuthFlow()
                    {
                        AuthorizationUrl = new Uri(Configuration["App:Authority"] + "/connect/authorize"),
                        TokenUrl = new Uri(Configuration["App:Authority"] + "/connect/token"),
                        Scopes = new Dictionary<string, string>
                        {
                            {"tyvan.aud", "tyvan.aud"}
                        }
                    }
                }
            });
            c.AddSecurityRequirement(new OpenApiSecurityRequirement
            {
                {
                    new OpenApiSecurityScheme
                    {
                        Reference = new OpenApiReference
                        {
                            Type = ReferenceType.SecurityScheme,
                            Id = "oauth2"
                        }
                    },
                    new string[] {}
                }
            });
        });
    }

    public static void UseSwaggerDoc(
        this WebApplication app, IConfiguration Configuration)
    {
        app.UseSwagger();
        app.UseSwaggerUI((Action<SwaggerUIOptions>)(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Your API v1");
                c.OAuthClientId(Configuration["App:ClientId"]);
                c.OAuthAppName("Swagger");
            })
        );
    }
}