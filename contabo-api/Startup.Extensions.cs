using contabo_api.Business;
using contabo_api.Database;
using contabo_api.Infrastructure;
using contabo_api.Repositories;
using MongoDB.Driver;

namespace contabo_api;

public static class StartupExtensions
{
    public static void AddCoreServices(this IServiceCollection services, IConfiguration config)
    {
        services.AddSingleton<IMongoDatabase>(c =>
        {
            // var mongoUrl = "mongodb://root:root@localhost:27017/drive";
            // var mongoDb = (new MongoClient(mongoUrl)).GetDatabase(new MongoUrl(mongoUrl).DatabaseName);
            // return new MongoDb(mongoDb);
            var mongoUrl = "mongodb://tyvan1:tyvan1@95.111.246.70:27017/drive";
            var mongoDb = (new MongoClient(mongoUrl)).GetDatabase(new MongoUrl(mongoUrl).DatabaseName);
            return mongoDb;
        });
        services.AddSingleton<DbContext>();
        services.AddScoped(typeof(IRepositoryDefault<>), typeof(RepositoryDefault<>));
        services.AddScoped<IFolderBusiness, FolderBusiness>();
        services.AddScoped<IFolderRepository, FolderRepository>();
        services.AddScoped<IFileRepository, FileRepository>();
        services.AddScoped<IFileBusiness, FileBusiness>();
    }
    public static void AddRequestContext(this IServiceCollection services)
    {
        services.AddScoped<IRequestContext, RequestContext>();
    }

    public static void UseRequestContext(this IApplicationBuilder app) => app.Use((Func<HttpContext, Func<Task>, Task>)((context, next) =>
    {
        ((RequestContext)context.RequestServices.GetRequiredService<IRequestContext>()).Context = context;
        return next();
    }));
}