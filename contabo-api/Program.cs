using contabo_api;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerUI;

var builder = WebApplication.CreateBuilder(args);


// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddCoreServices(builder.Configuration);
builder.Services.AddRequestContext();
builder.Services.AddSwaggerDoc(builder.Configuration);
builder.Services.AddIoftyAuthenticate(builder.Configuration);

builder.Services.AddCors(options =>
    {
        options.AddPolicy("AllowSpecificOrigin",
            builder =>
            {
                builder.WithOrigins("https://localhost:44411", "https://drive.iofty.online")
                       .AllowAnyHeader()
                       .AllowAnyMethod();
            });
    });


var app = builder.Build();
app.UseForwardedHeaders(new ForwardedHeadersOptions
{
    ForwardedHeaders = ForwardedHeaders.XForwardedProto
});
// Configure the HTTP request pipeline.
app.UseSwaggerDoc(builder.Configuration);
app.UseRequestContext();
app.UseCors("AllowSpecificOrigin");
app.UseAuthentication();
app.UseAuthorization();
app.MapControllers();

app.Run();