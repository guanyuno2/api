﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace contabo_api.Business.Extensions;
public static class StringHelper
{
    public static string CustomUrlEncode(this string str)
    {
        if (str.IsNullOrEmptyOrWhitespace())
            return str;

        var builder = new StringBuilder();

        foreach (var ch in str.ToList())
        {
            bool isAscii = ch <= sbyte.MaxValue;

            builder.Append(
                isAscii
                    ? ch.ToString()
                    : HttpUtility.UrlEncode(ch.ToString(), Encoding.UTF8)
            );
        }

        return builder.ToString();
    }

    public static ObjectId ToObjectId(this string str)
    {
        if (!ObjectId.TryParse(str, out ObjectId objectId))
            return ObjectId.Empty;

        return objectId;
    }

    public static string ToTrim(this string str)
    {
        return str.IsEmpty() ? str : str.Trim();
    }

    public static bool IsInLength(this string str, int min = 2, int max = 200)
    {
        return str.Length >= min && str.Length <= max;
    }

    public static bool IsEmpty(this string str)
    {
        return string.IsNullOrEmpty(str);
    }

    public static bool IsWhiteSpace(this string str)
    {
        return string.IsNullOrWhiteSpace(str);
    }

    public static bool IsNullOrEmptyOrWhitespace(this string str)
    {
        return string.IsNullOrEmpty(str) || string.IsNullOrWhiteSpace(str);
    }

    public static bool IsSameAs(this string str1, string str2)
    {
        return string.Compare(str1, str2, true) == 0;
    }

    public static byte[] ToByte(this string str)
    {
        if (str == null) return null;
        return System.Convert.FromBase64String(str);
    }

    public static string ToStripHTML(string input)
    {
        return Regex.Replace(input, "<.*?>|&.*?;", string.Empty);
    }

    public static string ToSubString(this string s, int length)
    {
        if (string.IsNullOrEmpty(s))
            return s;


        if (string.IsNullOrEmpty(s))
            return s;

        if (string.IsNullOrWhiteSpace(s))
            return s;

        var words = s.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

        if (words[0].Length > length)
            return s;

        var sb = new StringBuilder();
        foreach (var word in words)
        {
            if ((sb + word).Length > length)
                return string.Format("{0}", sb.ToString().TrimEnd(' '));
            sb.Append(word + " ");
        }

        return string.Format("{0}", sb.ToString().TrimEnd(' '));
    }

    public static List<long?> ToLong(this string str)
    {
        var value = str.ToTrim();

        if (IsEmpty(value)) return null;

        var listLong = value.Split(',').ToList();

        return listLong.Select(s => Int64.TryParse(s, out Int64 n) ? n : (Int64?)null).Distinct().ToList();

    }

    public static List<string> SplitByCommas(this string str)
    {
        var value = str.ToTrim();

        if (IsEmpty(value)) return null;

        return value.Split(',').Distinct().ToList();
    }

    public static DateTime? ToDateTime(this string str)
    {
        var value = str.ToTrim();

        if (IsEmpty(value)) return null;

        if (DateTime.TryParseExact(str, "dd/MM/yyyy hh:mm tt", CultureInfo.InvariantCulture, DateTimeStyles.None,
                out DateTime datetime))
            return datetime;

        return null;
    }

    public static string RemoveDiacritics(this String s)
    {
        String normalizedString = s.Normalize(NormalizationForm.FormD);
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < normalizedString.Length; i++)
        {
            Char c = normalizedString[i];
            if (c == 'đ') c = 'd';
            if (c == 'Đ') c = 'd';
            if (CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark)
                stringBuilder.Append(c);
        }

        return stringBuilder.ToString();
    }

    public static DateTime? ToDateTimeWithFormat(this String dateString, string dateFormat)
    {
        if (DateTime.TryParseExact(dateString, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None,
                out DateTime result))
        {
            return result;
        }

        return null;
    }

    public static string ToDate(this DateTime dateTime, string format = "dd/MM/yyyy hh:mm tt")
    {
        return dateTime.ToString(format);
    }
    public static string ConvertToUnSign(this string vn)
    {
        if (string.IsNullOrEmpty(vn)) return "";
        Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
        string temp = vn.Normalize(NormalizationForm.FormD);
        return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
    }

    public static string ToHours(this DateTime dateTime, string format = "HH:mm")
    {
        return dateTime.ToString(format);
    }
}
