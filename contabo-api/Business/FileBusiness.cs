using contabo_api.Business.Extensions;
using contabo_api.Infrastructure;
using contabo_api.Repositories;

namespace contabo_api.Business;

public class FileBusiness : IFileBusiness
{
    private readonly IFileRepository _rp;
    private readonly IRequestContext _ctx;
    public FileBusiness(IFileRepository rp, IRequestContext ctx)
    {
        _rp = rp;
        _ctx = ctx;
    }

    public async Task<List<FileModel>> GetMyFile(string id)
    {
        var data = await _rp.GetMyFile(id);
        return data?.Select(FileModel.ToModel).ToList();
    }

    public async Task<List<FileModel>> SearchFile(string query)
    {
        
        var existed = await _rp.SearchFile(query);
        return existed?.Select(FileModel.ToModel).ToList();
    }

    public async Task<FileModel> UploadFile(FileModel model)
    {
        if (model == null || model.Url.IsNullOrEmptyOrWhitespace())
        {
            _ctx.AddError("error");
            return null;
        }
        var entity = FileModel.ToEntity(model);
        await _rp.Add(entity);

        return FileModel.ToModel(entity);
    }
}