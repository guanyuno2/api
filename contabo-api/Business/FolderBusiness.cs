using contabo_api.Business.Extensions;
using contabo_api.Infrastructure;
using contabo_api.Repositories;

namespace contabo_api.Business;

public class FolderBusiness : IFolderBusiness
{
    private readonly IFolderRepository _rp;
    private readonly IRequestContext _ctx;
    public FolderBusiness(IFolderRepository rp, IRequestContext ctx)
    {
        _rp = rp;
        _ctx = ctx;
    }

    public async Task<FolderModel> CreateFolder(FolderModel body)
    {
        if (body == null || body.Name.IsNullOrEmptyOrWhitespace())
        {
            _ctx.AddError("Name is null");
            return null;
        }

        var entity = FolderModel.ToEntity(body);
        await _rp.Add(entity);

        return FolderModel.ToModel(entity);
    }

    public async Task<FolderModel> GetFolder(string id)
    {
        if (id.IsNullOrEmptyOrWhitespace())
        {
            _ctx.AddError("Id is null");
            return null;
        }
        var existed = await _rp.GetById(id);
        return FolderModel.ToModel(existed);
    }

    public async Task<List<FolderModel>> GetMyFolder(string id)
    {
        var data = await _rp.GetMyFolder(id);
        return data?.Select(FolderModel.ToModel).ToList();
    }

    public async Task<List<FolderModel>> GetPath(string id)
    {
        //case
        var allData = await _rp.GetAllMyFolder();
        var result = new List<FolderModel>();
        if (allData?.Count > 0)
        {
            var compareParentId = id;
            var isContinue = true;
            do
            {
                var corres = allData.FirstOrDefault(a => a.Id + "" == compareParentId);
                if (corres != null)
                {
                    result.Add(FolderModel.ToModel(corres));
                    if (corres.ParentId == null || corres.ParentId == default)
                        isContinue = false;
                    else
                        compareParentId = corres.ParentId + "";
                }
            } while (isContinue);
            result.Reverse();
        }
        return result;
    }

    public async Task<List<FolderModel>> SearchFolder(string query)
    {
        var existed = await _rp.SearchFolder(query);
        return existed?.Select(FolderModel.ToModel).ToList();
    }
}