using contabo_api.Repositories;
using MongoDB.Bson;

namespace contabo_api.Business;

public class FileModel : Trackable
{
    public string Url { get; set; }
    public string FileName { get; set; }
    public string ParentId { get; set; } = "";

    public static FileModel ToModel(MGFileModel model)
    {
        if (model == null) return null;

        return new FileModel()
        {
            Id = model.Id + "",
            Url = model.Url,
            FileName = model.FileName,
            ParentId = model.ParentId + "",
            CreatedAt = model.CreatedAt,
            CreatedBy = model.CreatedBy,
            UpdatedAt = model.UpdatedAt,
            UpdatedBy = model.UpdatedBy
        };
    }

    public static MGFileModel ToEntity(FileModel model)
    {
        if (model == null) return null;

        return new MGFileModel()
        {
            Id = ObjectId.TryParse(model.Id, out ObjectId id) ? id : ObjectId.Empty,
            Url = model.Url,
            FileName = model.FileName,
            ParentId = ObjectId.TryParse(model.ParentId, out ObjectId pid) ? pid : null,
            CreatedAt = model.CreatedAt,
            CreatedBy = model.CreatedBy,
            UpdatedAt = model.UpdatedAt,
            UpdatedBy = model.UpdatedBy
        };
    }
}