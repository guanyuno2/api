using contabo_api.Repositories;
using MongoDB.Bson;

namespace contabo_api.Business;

public class FolderModel : Trackable
{
    public string Name { get; set; }
    public string ParentId { get; set; } = "";

    public static FolderModel ToModel(MGFolderModel model)
    {
        if (model == null) return null;

        return new FolderModel()
        {
            Id = model.Id + "",
            Name = model.Name,
            ParentId = model.ParentId + "",
            CreatedAt = model.CreatedAt,
            CreatedBy = model.CreatedBy,
            UpdatedAt = model.UpdatedAt,
            UpdatedBy = model.UpdatedBy
        };
    }

    public static MGFolderModel ToEntity(FolderModel model)
    {
        if (model == null) return null;

        return new MGFolderModel()
        {
            Id = ObjectId.TryParse(model.Id, out ObjectId id) ? id : ObjectId.Empty,
            Name = model.Name,
            ParentId = ObjectId.TryParse(model.ParentId, out ObjectId pid) ? pid : null,
            CreatedAt = model.CreatedAt,
            CreatedBy = model.CreatedBy,
            UpdatedAt = model.UpdatedAt,
            UpdatedBy = model.UpdatedBy
        };
    }
}