namespace contabo_api.Business;

public interface IFolderBusiness
{
    Task<FolderModel> GetFolder(string id);
    Task<List<FolderModel>> GetMyFolder(string id);
    Task<List<FolderModel>> GetPath(string id);
    Task<List<FolderModel>> SearchFolder(string query);
    Task<FolderModel> CreateFolder(FolderModel model);
}