namespace contabo_api.Business;

public interface IFileBusiness
{
    Task<FileModel> UploadFile(FileModel model);
    Task<List<FileModel>> GetMyFile(string id);
    Task<List<FileModel>> SearchFile(string query);
}