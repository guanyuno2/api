﻿FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["contabo-api/contabo-api.csproj", "contabo-api/"]
RUN dotnet restore "contabo-api/contabo-api.csproj"
COPY . .
WORKDIR "/src/contabo-api"
RUN dotnet build "contabo-api.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "contabo-api.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "contabo-api.dll"]
